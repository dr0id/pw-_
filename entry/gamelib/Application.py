# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'Application.py' is part of pw29pw---_-
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The application module.

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module
import pygame

from gamelib import settings, resources
from gamelib.gamestates.loadcontext import LoadContext
from pyknic import context
from pyknic.pyknic_pygame.context import PygameInitContext, AppInitContext
from pyknic.pyknic_pygame.sfx import MusicPlayer

logger = logging.getLogger(__name__)
logger.debug("importing...")


class App(object):

    def __init__(self):
        self.music_player = MusicPlayer(settings.master_volume, settings.music_ended_pygame_event)
        self.screen = None

    def run(self):
        songs = resources.songs
        context.push(PygameInitContext(self._custom_display_init))
        context.push(AppInitContext(self.music_player, settings.path_to_icon, settings.title, songs=songs))

        c = LoadContext(self)
        context.push(c)

        context.set_deferred_mode(True)
        while context.length():
            top = context.top()
            if top:
                top.update(0, 0)
            context.update()  # handle deferred context operations

    def _custom_display_init(self, display_info, driver_info, wm_info):
        accommodated_height = int(display_info.current_h * 0.9)
        if accommodated_height < settings.screen_height:
            logger.info("accommodating height to {0}", accommodated_height)
            settings.screen_height = accommodated_height  # accommodate for title bar and task bar ?
        settings.screen_size = settings.screen_width, settings.screen_height
        logger.info("using screen size: {0}", settings.screen_size)

        # initialize the screen
        self.screen = pygame.display.set_mode(settings.screen_size, settings.screen_flags)

        # mixer values to return
        frequency = settings.MIXER_FREQUENCY
        channels = settings.MIXER_CHANNELS
        mixer_size = settings.MIXER_SIZE
        buffer_size = settings.MIXER_BUFFER_SIZE
        # pygame.mixer.pre_init(frequency, mixer_size, channels, buffer_size)
        return frequency, mixer_size, channels, buffer_size


logger.debug("imported")
