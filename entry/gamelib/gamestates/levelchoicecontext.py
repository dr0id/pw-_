# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'levelchoicecontext.py' is part of pw29pw---_-
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The level choice context.


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import json
import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module
import os
from collections import OrderedDict

import pygame

from gamelib import resources
from gamelib.gamestates.basegamecontext import BaseGameContext, BaseObject, GameSprite, Mouse, Button
from gamelib.gamestates.gamecontext import GameContext
from gamelib.gamestates.gameobjects import Pole, Puzzle, FlipPole, TechPole
from pyknic.events import Signal
from pyknic.mathematics import Point2, Vec2
from pyknic.pyknic_pygame.spritesystem import Camera, DefaultRenderer, TextSprite

logger = logging.getLogger(__name__)
logger.debug("importing...")

puzzles = [
    # Rect(178, 134, 800, 600)     178 <= x <= 978      134 <= y <= 734
    Puzzle(1, {2}, 0.91, resources.BUTTERFLY01, [Pole(800, 200, 0.1)], [Pole(800, 200, 0.1)],           "Drag magnet from left tray to fix image", "Press \"Fix\" to make changes permanent"),
    Puzzle(2, {3}, 0.92, resources.BUTTERFLY02, [Pole(200, 600, 0.1)], [Pole(800, 200, 0.1)],           "Small change  huge effect", "Adjusting the brightness helps find cracks"),
    Puzzle(3, {4}, 0.89, resources.BUTTERFLY03, [Pole(872, 540, 0.1), Pole(570, 438, 0.1)], [Pole(800, 200,0.1), Pole(800, 200,0.1)],           "Try using both magnets"),
    Puzzle(4, {5,6 ,7}, 0.70, resources.BUTTERFLY04, [Pole(950, 362, 0.1), Pole(790, 534, 0.5)], [Pole(800, 200,0.5), Pole(800, 200,0.1)],           "Not all magnets have same strength"),
    Puzzle(5, {6}, 0.73, resources.BUTTERFLY05, [Pole(247, 292, 0.3), Pole(446, 338, 0.5)], [Pole(800, 200,0.5), Pole(800, 200,0.1), Pole(800, 200,0.3)],           "There is one magnet too many", "Leave it in the tray"),
    Puzzle(6, {7}, 0.69, resources.BUTTERFLY06, [Pole(926, 607, 0.1), Pole(761, 136, 0.3), Pole(596, 390, 0.5)], [Pole(800, 200,0.1), Pole(446, 338, 0.3), Pole(446, 338, 0.5)],           "3 magnets make it fiddly"),
    Puzzle(7, {8}, 0.61, resources.BUTTERFLY07,
           [Pole(448, 459, 0.1), Pole(564, 450, 0.3), Pole(219, 319, 0.3), Pole(725, 700, 0.5)],
           [Pole(800, 200,0.1), Pole(761, 136, 0.3), Pole(761, 136, 0.3), Pole(761, 136, 0.5)],           "4 might be too much."),
    Puzzle(8, {9}, 0.53, resources.BUTTERFLY08, [Pole(636, 177, 0.1)], [FlipPole(800, 200,-0.1)],           "Magnets have two poles", "Use right mouse button to flip"),
    Puzzle(9, {10}, 0.54, resources.BUTTERFLY09, [Pole(762, 728, 0.1), Pole(973, 179, 0.1)], [FlipPole(800, 200,0.1), FlipPole(800, 200,-0.1)],           "Two poles each"),
    Puzzle(10, {11}, 0.51, resources.BUTTERFLY10, [Pole(955, 696, 0.2), Pole(543, 508, -0.4), Pole(487, 464, 0.6)], [FlipPole(800, 200,-0.2),FlipPole(800, 200,-0.4),FlipPole(800, 200,0.6)],           "Two poles and different strengths"),
    Puzzle(11, {12}, 0.49, resources.BUTTERFLY11, [Pole(800, 200, 0.1), Pole(295, 226, 0.3)], [FlipPole(800, 200, -0.1), Pole(800, 200,0.3)],           "Mixed polarities."),

    Puzzle(12, {13}, 0.33, resources.BUTTERFLY12, [Pole(302, 369, 0.8)], [TechPole(800, 200,0.1)],           "Tech magnets are adjustable", "use scroll to adjust"),
    Puzzle(13, {14, 15, 16}, 0.31, resources.BUTTERFLY13,
           [Pole(435, 199, 0.1), Pole(544, 670, 0.1)],
           [TechPole(800, 200,0.9), TechPole(800, 200,-0.3)],           "This is no Butter fly effect", "the outcome is not chaotic"),

    Puzzle(14, {15}, 0.11, resources.BUTTERFLY14,
           [Pole(874, 645, -0.6),Pole(282, 328, 0.3),Pole(647, 253, 0.1),Pole(288, 362, -0.1)],
           [TechPole(800, 200,0.4), FlipPole(282, 328,0.3), FlipPole(647, 253,0.1),Pole(800, 200,0.2),Pole(800, 200,0.1)],           "Patience", "fiddling around"),
    Puzzle(15, {16}, 0.12, resources.BUTTERFLY15,
           [Pole(641, 310, 0.9),Pole(321, 520, 0.1),Pole(245, 397, 0.2),Pole(751, 647, -0.3)],
           [TechPole(800, 200,0.1), FlipPole(800, 200,0.1), TechPole(800, 200,0.1), FlipPole(800, 200,0.3), Pole(800, 200, -0.1)],           "Too many variables?"),
    Puzzle(16, set(), 0.10, resources.BUTTERFLY16,
           [Pole(200, 160, -0.4), Pole(930, 170, 0.1),Pole(910, 700, 0.9),Pole(220, 680, -0.7)],
           [Pole(800, 200,0.1), FlipPole(800, 200,0.4), TechPole(800, 200,0.1), TechPole(800, 200,0.1) ],           "This is the last one", "hopfully you enjoyed the puzzles"),
]


# Rect(178, 134, 800, 600)     178 <= x <= 978      134 <= y <= 734


class Tile(BaseObject):

    def __init__(self, puzzle, choosable):
        BaseObject.__init__(self, -1)
        self._choosable = choosable
        self.even_choosable_changed = Signal()
        self.puzzle = puzzle
        self.event_activate = Signal()
        self.event_solved_state_changed = Signal()
        self.event_focus_changed = Signal()
        self.has_focus = False

    @property
    def choosable(self):
        return self._choosable

    @choosable.setter
    def choosable(self, value):
        if self._choosable != value:
            self._choosable = value
            self.even_choosable_changed.fire(self)

    def click(self, button):
        if button == 1 and self.choosable:  # LMB
            self.event_activate.fire(self)

    def on_focus(self, mouse):
        self.has_focus = True
        self.event_focus_changed.fire(self)

    def on_focus_lost(self, mouse):
        self.has_focus = False
        self.event_focus_changed.fire(self)

    def update_state(self):
        self.event_solved_state_changed.fire(self)

PROGRESS_FILE_PATH = "savegame.json"

class LevelChoiceContext(BaseGameContext):

    def __init__(self, app):
        BaseGameContext.__init__(self, app)
        self.renderer = DefaultRenderer()
        self.screen_rect = rect = self.app.screen.get_rect()
        self._cam = Camera(rect, Point2(rect.centerx, rect.centery))
        self.current_selected_tile = None
        self.solved_state = dict([(p.puzzle_id, p.solved) for p in puzzles])
        self.choosable_puzzle_ids = set([1])
        self._mouse = Mouse()
        self.tiles = []

    def enter(self):
        BaseGameContext.enter(self)
        # todo load solve state from disk  (or load it via resources!)
        if os.path.exists(PROGRESS_FILE_PATH):
            with open(PROGRESS_FILE_PATH, "r") as fp:

                self.solved_state = json.load(fp)
                self.solved_state = dict([(int(k), v) for k, v in self.solved_state.items()])
                logger.debug("savegame loaded: %s", self.solved_state)

        for p in puzzles:
            p.solved = self.solved_state.setdefault(p.puzzle_id, False)
            if p.solved:
                self.choosable_puzzle_ids.update(p.releases)

        puzzles.sort(key=lambda _p: _p.puzzle_id)
        dx = 200
        dy = 150
        x = dx
        y = -dy/2
        for idx, p in enumerate(puzzles):
            if idx % 4 == 0:
                x = dx
                y += dy
            self._create_tile(p, Point2(x, y))
            x += dx

        self._create_quit_button()
        if len(self.choosable_puzzle_ids) == 1:
            self.on_activate_tile([t for t in self.tiles if t.puzzle.puzzle_id == puzzles[0].puzzle_id][0])


    def _create_quit_button(self):
        on_off_button = Button(Point2(*self.app.screen.get_rect().inflate(0, -40).midbottom), self._quit)
        btn_img = resources.loaded_resources[resources.OFFBUTTON].data
        buttons_spr = GameSprite(btn_img, on_off_button.position, on_off_button, anchor='center')
        self.renderer.add_sprite(buttons_spr)

    def exit(self):
        BaseGameContext.exit(self)
        # todo save solved state to disk
        with open(PROGRESS_FILE_PATH, "w") as fp:
            json.dump(self.solved_state, fp)

    def resume(self):
        BaseGameContext.resume(self)
        logger.debug("current solved %s", self.current_selected_tile.puzzle.solved)
        self.solved_state[self.current_selected_tile.puzzle.puzzle_id] = self.current_selected_tile.puzzle.solved
        self.current_selected_tile.update_state()
        if self.current_selected_tile.puzzle.solved:
            self.choosable_puzzle_ids.update(self.current_selected_tile.puzzle.releases)
        for t in self.tiles:
            if t.puzzle.puzzle_id in self.choosable_puzzle_ids:
                t.choosable = True

    def suspend(self):
        BaseGameContext.suspend(self)

    def update_step(self, delta, sim_time, *args):
        self._handle_events()

    def _handle_events(self):
        BaseGameContext.update_music_player(self)  # this has to be called before getting all events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self._quit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self._quit()
            elif event.type == pygame.MOUSEMOTION:
                # pos, rel, buttons
                objects = self._get_objects_at(event.pos)
                if event.buttons != (0, 0, 0):
                    # drag!
                    self._mouse.on_drag(event.pos, event.rel, event.buttons, objects)
                else:
                    self._mouse.check_hover(objects, event.pos)

            elif event.type == pygame.MOUSEBUTTONDOWN:
                logger.debug(f"py mB down {event.button}")
                objects = self._get_objects_at(event.pos)
                self._mouse.press(event.button, event.pos, objects)
            elif event.type == pygame.MOUSEBUTTONUP:
                logger.debug(f"py mB up {event.button}")
                objects = self._get_objects_at(event.pos)
                self._mouse.release(event.button, event.pos, objects)

    def _get_objects_at(self, pos):
        sprites = self.renderer.get_sprites_at_tuple(pos)
        objects = [s.obj for s in sprites]
        objects = list(OrderedDict.fromkeys(objects))  # make them distinct but keep order
        return objects

    def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
        self.renderer.draw(self.app.screen, self._cam, (0, 0, 0), do_flip)

    def _create_tile(self, puzzle, position):
        t = Tile(puzzle, puzzle.puzzle_id in self.choosable_puzzle_ids)
        t.event_activate.add(self.on_activate_tile)
        self.tiles.append(t)

        frame_spr = GameSprite(resources.loaded_resources[resources.TILE_FRAME].data, position, t, z_layer=1)
        self.renderer.add_sprite(frame_spr)

        broken_spr = GameSprite(resources.loaded_resources[resources.TILE_BROKEN].data, position, t)
        self.renderer.add_sprite(broken_spr)

        fixed_spr = GameSprite(resources.loaded_resources[resources.TILE_FIXED].data, position, t)
        self.renderer.add_sprite(fixed_spr)

        hover_spr = GameSprite(resources.loaded_resources[resources.TILE_HIGHLIGHT].data, position, t, z_layer=-1)
        self.renderer.add_sprite(hover_spr)

        shade_spr = GameSprite(resources.loaded_resources[resources.TILE_SHADE].data, position, t, z_layer=2)
        self.renderer.add_sprite(shade_spr)

        f = resources.loaded_resources[resources.TILE_FONT].data
        txt_spr = TextSprite(f"{t.puzzle.puzzle_id} - Integrity: {t.puzzle.integrity}", position+Vec2(-75, 50), font=f, anchor="topleft", z_layer=10)
        txt_spr.obj = t
        self.renderer.add_sprite(txt_spr)

        def _update_solved(tile):
            logger.debug("Tile state changed! update sprites accordingly %s", tile.puzzle.solved)
            fixed_spr.visible = True  if tile.puzzle.solved else False
            broken_spr.visible = not fixed_spr.visible
        _update_solved(t)
        t.event_solved_state_changed.add(_update_solved)

        def _update_choosable(tile):
            logger.debug("Tile choosable changed %s", tile.choosable)
            shade_spr.visible = False if tile.puzzle.puzzle_id in self.choosable_puzzle_ids else True
        _update_choosable(t)
        t.even_choosable_changed.add(_update_choosable)

        def _update_focus(tile):
            logger.debug("Tile focus changed %s", tile.has_focus)
            hover_spr.visible = tile.has_focus
        _update_focus(t)
        t.event_focus_changed.add(_update_focus)


    def on_activate_tile(self, tile):
        self.current_selected_tile = tile
        self.push(GameContext(self.app, self.current_selected_tile.puzzle))

    def _quit(self):
        self.pop()


logger.debug("imported")
