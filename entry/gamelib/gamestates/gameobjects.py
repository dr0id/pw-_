# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'gameobjects.py' is part of pw29pw---_-
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
TODO: module description

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

__all__ = []  # list of public visible parts of this module

import pygame

from gamelib.gamestates.basegamecontext import BaseObject, InteractionOptions
from gamelib.settings import KIND_POLE
from pyknic.events import Signal
from pyknic.mathematics import Point2

logger = logging.getLogger(__name__)
logger.debug("importing...")

POLE_CAPABILITY_NONE = 0
POLE_CAPABILITY_FLIP = 1
POLE_CAPABILITY_FULL = 2

POLARITY_STEP = 0.1


class Pole(BaseObject):

    def __init__(self, x, y, polarity, capability=POLE_CAPABILITY_NONE):
        BaseObject.__init__(self, KIND_POLE)
        self.capability = capability
        self._origin_pos = None
        self.position = Point2(x, y)
        assert pygame.Rect(178, 134, 800, 600).collidepoint(x, y), "point outside field"
        self.polarity = polarity
        self.radius = 20
        self.event_drag_changed = Signal()
        self.is_dragging = False
        self.event_polarity_changed = Signal()

    def on_focus(self, mouse):
        mouse.interaction_option = InteractionOptions.Grab

    def on_focus_lost(self, mouse):
        mouse.interaction_option = InteractionOptions.Default

    def on_drop_failed(self, mouse):
        self.position.copy_values(self._origin_pos)

    def on_drag_start(self, mouse, button):
        """
        Called when dragging starts.
        :param button: The button of the mouse pressed
        :param mouse: The mouse object
        :return: True if dragging is enable, False otherwise.
        """
        if button == 1:
            self.is_dragging = True
            self.event_drag_changed.fire(self)
            mouse.interaction_option = InteractionOptions.Drag
            # if self._origin_pos is None:
            self._origin_pos = self.position.clone()
            return True  # no dragging, return True to allow dragging
        return False

    def on_drag_end(self, mouse):
        self.is_dragging = False
        self.event_drag_changed.fire(self)

    def on_drag(self, mouse, x, y, dx, dy):
        self.position.x += dx
        self.position.y += dy

    def click(self, button):
        logger.debug(f"mb click {button}")
        if button == 3 and (self.capability == POLE_CAPABILITY_FLIP or self.capability == POLE_CAPABILITY_FULL):  # RMB
            self.polarity = -self.polarity
            self.event_polarity_changed.fire(self)
        elif button == 4 and self.capability == POLE_CAPABILITY_FULL:  # scroll up
            self.polarity = round(self.polarity + POLARITY_STEP, 1)
            self.event_polarity_changed.fire(self)
        elif button == 5 and self.capability == POLE_CAPABILITY_FULL:  # scroll down
            self.polarity = round(self.polarity - POLARITY_STEP, 1)
            self.event_polarity_changed.fire(self)


class FlipPole(Pole):

    def __init__(self, x, y, polarity):
        Pole.__init__(self, x, y, polarity, POLE_CAPABILITY_FLIP)


class TechPole(Pole):
    def __init__(self, x, y, polarity):
        Pole.__init__(self, x, y, polarity, POLE_CAPABILITY_FULL)


class Puzzle(object):

    def __init__(self, puzzle_id, releases, integrity, resource_id, solution_poles, available_poles, instruction1="",
                 instruction2=""):
        assert len(solution_poles) <= len(available_poles), f"not enough poles for solution! {len(solution_poles)} < {len(available_poles)}"
        self.releases = releases
        self.puzzle_id = puzzle_id
        self.resource_id = resource_id
        self.instruction1 = instruction1
        self.instruction2 = instruction2
        self.solution_poles = solution_poles
        self.available_poles = available_poles
        self.integrity = integrity  # 0.0-1.0
        self.solved = False


logger.debug("imported")
