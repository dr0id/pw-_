# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'introcontext.py' is part of pw29pw---_-
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The intro and story context.


.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function, division

import logging

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(_d) for _d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2020"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module
import pygame

from gamelib import resources
from gamelib.gamestates.basegamecontext import BaseGameContext
from gamelib.gamestates.levelchoicecontext import LevelChoiceContext
from pyknic.mathematics import Point2
from pyknic.pyknic_pygame.spritesystem import DefaultRenderer, Camera, Sprite

logger = logging.getLogger(__name__)
logger.debug("importing...")


class IntroContext(BaseGameContext):

    def __init__(self, app):
        BaseGameContext.__init__(self, app)
        self.renderer = DefaultRenderer()
        self.screen_rect = rect = self.app.screen.get_rect()
        self._cam = Camera(rect, Point2(rect.centerx, rect.centery))

    def enter(self):
        BaseGameContext.enter(self)
        img = resources.loaded_resources[resources.STORY].data
        spr = Sprite(img, self._cam.position.clone())
        self.renderer.add_sprite(spr)

    def exit(self):
        BaseGameContext.exit(self)

    def update_step(self, delta, sim_time, *args):
        self._handle_events()

    def _handle_events(self):
        BaseGameContext.update_music_player(self)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                self._next()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                self._next()

    def _next(self):
        self.exchange(LevelChoiceContext(self.app))

    def draw(self, dt, sim_dt, do_flip=True, interpolation_factor=1.0):
        self.renderer.draw(self.app.screen, self._cam, (0, 0, 0), do_flip)


logger.debug("imported")
