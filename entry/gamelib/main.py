# -*- coding: utf-8 -*-
"""
This it the main module. This is the main entry point of your code (put in the main() method).
"""
from __future__ import division, print_function

import logging

logger = logging.getLogger(__name__)





def main():
    # put here your code
    from gamelib import settings
    logging.getLogger().setLevel(settings.log_level)

    import pyknic
    pyknic.logs.log_environment()
    pyknic.logs.print_logging_hierarchy()

    from gamelib.Application import App
    app = App()
    app.run()

    logger.info('Exit game.')


# this is needed in order to work with py2exe
if __name__ == '__main__':
    main()
