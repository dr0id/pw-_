﻿Licences

iconfinder_icon_3_high_five_329409.png
    https://www.iconfinder.com/icons/329409/five_gesture_hand_high_icon
    http://creativecommons.org/licenses/by/3.0/
    Modified to be white!

iconfinder_icon_22_one_finger_329391.png
    https://www.iconfinder.com/icons/329391/finger_gesture_hand_one_icon
    http://creativecommons.org/licenses/by/3.0/
    modified to grab hand: iconfinder_icon_22_one_finger_329391_modified.png
    modified to be white!
    iconfinder_icon_22_one_finger_329391_modified_nodrop.png

iconfinder_icon_27_one_finger_click_329387.png
    https://www.iconfinder.com/icons/329387/click_finger_gesture_hand_one_icon
    http://creativecommons.org/licenses/by/3.0/

roman-font.jpg
    https://www.kyledesigns.com/gift-engraving-styles-formats-for-best-results/
    Cut out 'S' and 'N'

magnet.png
    https://www.containerstore.com/s/office/message-boards-accessories/three-by-three-stainless-small-snap-strong-magnets/12d?productId=11006484
    Cut out one magnet

on/off and fix Button:
    https://global2019-static-cdn.kikuu.com/upload-productImg-5298449767312241_320_234.jpg?
    on/off icon from https://www.iconfinder.com/icons/5298743/off_on_power_icon

Fonts:
    both ldc font are from:
    https://all-free-download.com/font/download/led_lcd_123_18557.html?q=lcd+


Songs:
    carefree-by-kevin-macleod-from-filmmusic-io.ogg
        Carefree by Kevin MacLeod
        Link: https://incompetech.filmmusic.io/song/3476-carefree
        License: http://creativecommons.org/licenses/by/4.0/

    anguish-by-kevin-macleod-from-filmmusic-io.ogg
        Anguish by Kevin MacLeod
        Link: https://incompetech.filmmusic.io/song/3373-anguish
        License: http://creativecommons.org/licenses/by/4.0/

db5dm5y-81e6240d-050d-4b5d-be12-8271f6328427.jpg
    https://www.deviantart.com/thundersplash/art/Butter-Fly-674161270

36838807772_737b309522_o.png
    https://c1.staticflickr.com/5/4355/36838807772_737b309522_o.png

Butterflies from:
    https://www.pexels.com/photo/nature-orange-butterfly-silver-bordered-fritillary-33073/
