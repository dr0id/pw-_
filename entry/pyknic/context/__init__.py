# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file is part of HG_pyknic
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
Context management.

Limitations:
    pop can be called from anywhere, also from within a update method of an
    active (topmost) context. After pop-ing it the active context is not the
    topmost context anymore, but can still modify the context stack (since
    it is still in the update method). -> use deferred mode!


"""

# Versioning scheme based on: 
#       http://en.wikipedia.org/wiki/Versioning#Designating_development_stage
#
#   +-- api change, probably incompatible with older versions
#   |     +-- enhancements but no api change
#   |     |
# major.minor[.build[.revision]]
#                |
#                +-|* 0 for alpha (status)
#                  |* 1 for beta (status)
#                  |* 2 for release candidate
#                  |* 3 for (public) release
#
# For instance:
#     * 1.2.0.1 instead of 1.2-a
#     * 1.2.1.2 instead of 1.2-b2 (beta with some bug fixes)
#     * 1.2.2.3 instead of 1.2-rc (release candidate)
#     * 1.2.3.0 instead of 1.2-r (commercial distribution)
#     * 1.2.3.5 instead of 1.2-r5 (commercial distribution with many bug fixes)
from __future__ import print_function, division
import logging

# noinspection PyProtectedMember
from pyknic.context._context_stack import *
# noinspection PyProtectedMember
from pyknic.context._context import *
from pyknic.context import transitions

logger = logging.getLogger(__name__)
logger.debug("importing...")


__version__ = "1.0.1.0"
__author__ = "DR0ID (C) 2011"

__copyright__ = "DR0ID @ Copyright 2011"
__credits__ = ["Cosmologicon", "Gummbum", "DR0ID"]
__license__ = "New BSD license"
__maintainer__ = "DR0ID"

__all__ = ["Context", "push", "pop", "top", "length", "print_stack", "transitions"]

# this is here to break the circular dependencies: transitions -> context -> context_stack -> transitions
# makes it harder for an IDE to provide those methods
Context.push = staticmethod(_context_stack.push)
Context.pop = staticmethod(_context_stack.pop)
Context.top = staticmethod(_context_stack.top)
Context.get_stack_length = staticmethod(_context_stack.length)
Context.exchange = staticmethod(_context_stack.exchange)

logger.debug("imported")
