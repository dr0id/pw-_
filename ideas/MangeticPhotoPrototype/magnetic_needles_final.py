# Copyright 2006 DR0ID <dr0id@bluewin.ch> http://mypage.bluewin.ch/DR0ID
#
#
#
"""
#TODO: documentation!
"""

__author__ = "$Author: DR0ID $"
__version__ = "$Revision: 7 $"
__date__ = "$Date: 2006-09-02 22:51:00 +0200 (Sa, 02 Sep 2006) $"

import math
import random
import sys
import functools

import pygame


class Needle(object):
    
    def __init__(self, pos, image, size=1.0):
        self.pos = pos
        self.angle = 0
        self._old_angle = -1
        self.solution_angle = 0
        self._image = image
        self.image = image
        self.rect = image.get_rect(center=pos)
        self.size = size
        self.stock = []  # [angle] -> image

        self.set_image(image, size, 0)

    def set_image(self, image, size, solution_angle):
        self.solution_angle = solution_angle
        self._image = image
        self.size = size
        self.rect = image.get_rect(center=self.pos)
        # for a in range(360):
        #     self.stock.append(pygame.transform.rotozoom(image, a, size))
        # rounded_angle = solution_angle #int(math.degrees(math.atan2(y, x)))
        # self.stock = self.stock[-rounded_angle:] + self.stock[:-rounded_angle]

    def update(self, others):
        px, py = self.pos
        fx = 0
        fy = 0
        for ox, oy, of in others:
            dx = px - ox
            dy = py - oy
            d = dx * dx + dy * dy
            if d == 0:
                continue
            fx -= of / d * (-px + ox)
            fy -= of / d * (py - oy)

        self.angle = int(math.degrees(math.atan2(fy, fx)))
        if self.angle != self._old_angle:
            self._old_angle = self.angle
            a = (self.angle - self.solution_angle) % 360

            if True:
                # alpha blended, very hard
                self.image = pygame.Surface(self._image.get_size(), flags=pygame.SRCALPHA)
                self.image.blit(self._image, (0, 0))
                c = pygame.Color(255, 255, 255, 255)
                h, s, v, _ = c.hsva
                aa = 360 - a
                if aa < 180:
                    alpha = 100 - int(aa / 180.0 * 100)
                else:
                    alpha = int((aa - 180) / 180.0 * 100)
                c.hsva = (h, s, v, alpha)
                self.image.fill(c, None, pygame.BLEND_RGBA_MULT)  # todo maybe use another field for alpha?
                self.image = pygame.transform.rotozoom(self.image, a, self.size)
                self.rect = self.image.get_rect(center=self.pos)
            else:
                # rotational
                self.image = pygame.transform.rotozoom(self._image, a, self.size)
                self.rect = self.image.get_rect(center=self.pos)

        # self.angles = [self.get_angle(*x) for x in diff]
        # self.rects = list(map(lambda i, p: self.stock[i].get_rect(center=p), self.angles, self.needlePos))


class NeedleGroup2(object):

        def __init__(self, image, size):
            self._needles = []
            self._image = image
            self._size = size

        def add(self, pos):
            self._needles.append(Needle(pos, self._image, self._size))

        def update(self, polarity, others=[]):
            if polarity == 0 and len(others) == 0:
                return
            others = others.copy()
            mx, my = pygame.mouse.get_pos()
            mx, my = snap_to_grid(mx, my)
            others.append((mx, my, polarity))

            for n in self._needles:
                n.update(others)

        def draw(self, screen):
            for n in self._needles:
                screen.blit(n.image, n.rect)

class NeedleGroup:

    def __init__(self, image, size):
        self.needlePos = []  # [[x1,y1],[],[]]
        self.angles = []  # [angle1, ...]
        self.rects = []

        self.stock = []

        # rotated needle images
        # for a in range(360):
        #     self.stock.append(pygame.transform.rotozoom(image, a, size))

        # use colors for each angle
        rect = pygame.Rect(0, 0, 220, 220)
        sr = pygame.Rect(110, 98, 210, 5)
        # rect = pygame.Rect(0, 0, 38, 38)
        # sr = pygame.Rect(19, 18, 38, 3)
        c = pygame.Color(0, 0, 0, 255)
        c.hsva = (0, 100, 100, 50)
        for a in range(360):
            c.hsva = (a, 100, 100, 50)
            s = pygame.Surface(rect.size, flags=pygame.SRCALPHA)
            s.fill(c)
            s.fill((0, 0, 0), sr)
            self.stock.append(pygame.transform.rotozoom(s, a, size))

    def add(self, pos):
        self.angles.append(0)
        self.needlePos.append(pos)

    def update(self, polarity, others=[]):
        mx, my = pygame.mouse.get_pos()
        oths = others + [(mx, my, polarity)]
        # ox, oy, of = zip(*((f*x, f*y, f) for x, y, f in oths))
        # # diff = [(p[1] - my, -p[0] + mx) for p in self.needlePos]
        # # diff = [(py - my, -px + mx)  for px, py in self.needlePos]
        # of_sum = sum(of)
        # diff = [(of_sum * py - sum(oy), of_sum * -px + sum(ox))  for px, py in self.needlePos]
        # # f1*(p - a) + f2*(p - b) + f3*(p - c) =    (f1+f2+f3)*p - f1*a - f2*b - f3*c
        # # f1*(p - a) + f2*(p - b) + f3*(p - c) =    (f1+f2+f3)*p - (f1*a + f2*b + f3*c)
        diff = []
        for px, py in self.needlePos:
            fx = 0
            fy = 0
            for ox, oy, of in oths:
                dx = px - ox
                dy = py - oy
                d = dx * dx + dy * dy
                if d == 0:
                    continue
                fx -= of / d * (-px + ox)
                fy -= of / d * (py - oy)
            diff.append((fy, fx))

        self.angles = [self.get_angle(*x) for x in diff]
        self.rects = list(map(lambda i, p: self.stock[i].get_rect(center=p), self.angles, self.needlePos))

    @functools.lru_cache(maxsize=800*600)
    def get_angle(self, y, x):
        return int(math.degrees(math.atan2(y, x)))

    def draw(self, scr):
        list(map(lambda i, p: scr.blit(self.stock[i], p), self.angles, self.rects))


def snap_to_grid(x, y, grid_size=5):
    return x // grid_size * grid_size, y // grid_size * grid_size

def main():
    pygame.display.init()
    scr = pygame.display.set_mode((800, 600))

    # init stuff
    needleImg = pygame.image.load("needle.png").convert()
    # needleImg.set_colorkey()

    needleImg.set_colorkey((255, 255, 255, 255), pygame.RLEACCEL)

    size = 0.15
    needles = NeedleGroup2(needleImg, size)
    # needles = NeedleGroup(needleImg, 1.0)

    count = 0
    gridstep = 25
    r = pygame.Rect(0, 0, gridstep, gridstep)
    rects = []
    for y in range(gridstep, 600, gridstep):
        for x in range(gridstep, 800, gridstep):
            needles.add((x, y))
            count += 1
            r.center = (x, y)
            rects.append(r.copy())

    # init puzzle
    solution_poles = [(300, 300, -1), (500, 300, 1)]
    # solution_poles = [ (490, 300, 1)]
    solution_poles = []
    for i in range(2):
        x = random.randint(10, 790)
        y = random.randint(10, 590)
        p = (random.randint(0, 20) - 10) / 10.0
        x, y = snap_to_grid(x, y)
        solution_poles.append((x,y,p))

    needles.update(0, solution_poles)
    bg_img = pygame.image.load("butterfly.jpg").convert()
    # bg_img = pygame.image.load("needle.png").convert()
    # bg_img.set_colorkey((255, 255, 255))
    bg_img = pygame.transform.scale(bg_img, (800, 600))
    for r in rects:
        needle = None
        for n in needles._needles:
            if n.pos == r.center:
                needle = n
                break
        if needles is None:
            print("no needle found for ", r.center)
            continue
        # needle.set_image(needleImg, size, needle.angle)
        s = pygame.Surface((gridstep, gridstep), flags=pygame.SRCALPHA)
        s.blit(bg_img, (0, 0), r)
        needle.set_image(s, 1.0, needle.angle)

    print("needles count: ", count)

    looping = True
    clock = pygame.time.Clock()
    tick = clock.tick
    needles.update(1)
    needles.draw(scr)
    pygame.display.flip()
    scr_flip = pygame.display.flip
    set_caption = pygame.display.set_caption
    others = []  # start with no poles
    polarity = 0
    while looping:
        for event in pygame.event.get():
            ##            event = pygame.event.wait()
            if event.type == pygame.QUIT:
                looping = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    looping = False
                elif event.key == pygame.K_SPACE:
                    polarity = 0
                    # info = needles.get_angle.cache_info()
                    # print(info, info.hits /float(info.misses))
                elif event.key == pygame.K_b:
                    print(solution_poles, "current: ", others)
            elif event.type == pygame.MOUSEMOTION:
                print(event.pos)
                pass
            elif event.type == pygame.MOUSEBUTTONDOWN:
                print("mousebutton", event.button)
                if event.button == 5:
                    polarity -= 0.1
                    print("polarity", polarity)
                elif event.button == 4:
                    polarity += 0.1
                    print("polarity", polarity)
                elif event.button == 1:  # left click
                    mx, my = pygame.mouse.get_pos()
                    mx, my = snap_to_grid(mx, my)
                    others.append((mx, my, polarity))
                    polarity = 0
                    print("add pole at",  others[-1], "    reset polarity to 0")
                elif event.button == 3:  # right click
                    nearest = None
                    n_dist = sys.maxsize
                    mx, my = pygame.mouse.get_pos()
                    for o in others:
                        ox, oy, of = o
                        dx = mx - ox
                        dy = my - oy
                        d = dx * dx + dy * dy
                        if d < n_dist:
                            nearest = o
                            n_dist = d
                    if nearest:
                        others.remove(nearest)
                        print("removed pole at",  nearest)


        needles.update(polarity, others)
        needles.draw(scr)
        scr_flip()
        # scr.fill((255, 255, 255))
        scr.fill((0, 0, 0))

        tick()
        set_caption("fps: " + str(clock.get_fps()) + " arrows count:" + str(count))

    # loop end
    pygame.quit()


if __name__ == '__main__':
    main()
